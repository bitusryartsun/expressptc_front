// // Import jQuery module (npm i jquery)
import $ from 'jquery'
window.jQuery = $
window.$ = $
import slick from 'slick-carousel'

import noUiSlider from 'nouislider';
// // Import vendor jQuery plugin example (not module)
// require('~/app/libs/mmenu/dist/mmenu.js')

document.addEventListener('DOMContentLoaded', () => {

	document.querySelector('.warehouseSelect__inner').addEventListener('click',function (event){
	    document.querySelector('.warehouseSelect').classList.toggle('active');

    });

	// SLIDER FILTER SHOP
	var slider = document.getElementById('slider-range');
	if(slider){
		noUiSlider.create(slider, {
			start: [20, 700],
			connect: true,
			range: {
				'min': 20,
				'max': 700
			}
		});

		slider.noUiSlider.on('update', function (values, handle) {
			console.log(values);
			$('#sliderOf').val(Number(values[0]).toFixed(0))
			$('#sliderTo').val(Number(values[1]).toFixed(0))
		});

		$('#sliderOf').change(function (event) {
			let value = event.target.value;
			let valueOther = $('#sliderTo').val();
			slider.noUiSlider.set([value,valueOther])
		})
		$('#sliderTo').change(function (event) {
			let value = event.target.value
			let valueOther = $('#sliderOf').val();
			slider.noUiSlider.set([valueOther,value])
		})
	}
	// END


	// NAVBAR STICKY
	$(window).scroll(function (event) {
		let scrollTop = $(this).scrollTop();
		if(scrollTop > 100){
			$('.topNav').addClass('sticky')
		}else{
			$('.topNav').removeClass('sticky')
		}
	})
	//CAROUSELS
	let sliderSlug = '.categories';
	$(sliderSlug + ' .slider').slick({
		infinite: true,
		slidesToShow: 3,
		centerPadding: '0',
		prevArrow: $('.categories .prev'),
		nextArrow: $('.categories .next'),
	});

	let sliderReviewsSlug = '.reviews';
	$(sliderReviewsSlug + ' .slider').slick({
		infinite: false,
		slidesToShow: 4,
		centerPadding: '0',
		prevArrow: $('.reviews .prev'),
		nextArrow: $('.reviews .next'),
		variableWidth: true
	});
	let sliderShopSlug = '.shopHeader';
	$('.shopHeader__slider').slick({
		infinite: false,
		slidesToShow: 8,
		centerPadding: '0',
		prevArrow: $(sliderShopSlug + ' .prev'),
		nextArrow: $(sliderShopSlug + ' .next'),
	});
	// END

	// FAQ COLLAPSE
	$('.faqItem').click(function (event) {
		$(this).find('.faqItem__body').slideToggle('slow');
	})
	//	END

})
